#include <vector> 
#include <stdio.h>
using namespace std;

struct evt 
{
    vector<int> ints;
    int iter_pos = 0;
    void Remove (int remove_pos)
    {
        // Anything already existing we haven't iterated through should not be skipped.
        ints[remove_pos] = ints[ints.size() - 1];
        ints.pop_back();
        if (remove_pos <= iter_pos) 
        {
            if (remove_pos < iter_pos)
            {
                int already_iterated = ints[iter_pos];
                ints[iter_pos] = ints[remove_pos];
                ints[remove_pos] = already_iterated;
            }
            iter_pos--;
        }
    }
    void Add (int adding_value)
    {
        //ints.push_back(adding_value);
        
        // Anything added during iteration shouldn't show during same iteration.
        iter_pos++;
        ints.push_back(ints[iter_pos]);
        ints[iter_pos] = adding_value;
    }
    
    evt ()
    {
        for (int i = 0; i < 10; i++) ints.push_back(i);
    }
    
    void show ()
    {
        bool first = true;
        for (int i : ints)
        {
            if (first) first = false;
            else printf(", ");
            printf("%d",i);
        }
        putchar('\n');
    }
    
    void test ()
    {
        show();
        
        Remove(4);
        
        show();
        
        putchar('<');
        bool first = true;
        for (iter_pos = 0; iter_pos < ints.size(); iter_pos++)
        {
            int value = ints[iter_pos];
            
            if (first) first = false;
            else printf(", ");
            printf("%d", value);
            
            if (value == 6) 
            {
                putchar('X');
                Remove(iter_pos);
            }
        }
        iter_pos = 0;
        printf(">\n");
        
        show();
        
        putchar('<');
        first = true;
        for (iter_pos = 0; iter_pos < ints.size(); iter_pos++)
        {
            int value = ints[iter_pos];
            
            if (first) first = false;
            else printf(", ");
            printf("%d", value);
            
            if (value == 8) 
            {
                putchar('*');
                Remove(0);
            }
        }
        iter_pos = 0;
        printf(">\n");
        
        show();
        
        putchar('<');
        first = true;
        for (iter_pos = 0; iter_pos < ints.size(); iter_pos++)
        {
            int value = ints[iter_pos];
            
            if (first) first = false;
            else printf(", ");
            printf("%d", value);
            
            if ((value & 1) == 0) 
            {
                putchar('+');
                Add(value * 2 + 1);
            }
        }
        iter_pos = 0;
        printf(">\n");        
        
        show();
        
        Remove(2);
        
        show();
        
        while (ints.size())
        {
            putchar('<');
            first = true;
            for (iter_pos = 0; iter_pos < ints.size(); iter_pos++)
            {
                int value = ints[iter_pos];
                
                if (first) first = false;
                else printf(", ");
                printf("%d", value);
                
                if (iter_pos == ints.size() - 1) 
                {
                    putchar('X');
                    Remove(iter_pos);
                }
            }
            iter_pos = 0;
            printf(">\n");
            
            show();
        }
        
        putchar('<');
        first = true;
        for (iter_pos = 0; iter_pos < ints.size(); iter_pos++)
        {
            int value = ints[iter_pos];
            
            if (first) first = false;
            else printf(", ");
            printf("%d", value);
            
            if (iter_pos == ints.size() - 1) 
            {
                putchar('X');
                Remove(iter_pos);
            }
        }
        iter_pos = 0;
        printf(">\n");
        
        show();
    }
};

int main ()
{
    evt e;
    e.test();
    return 0;
}