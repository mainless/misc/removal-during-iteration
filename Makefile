default: test.exe

OBJS = $(patsubst %.cpp,%.o,$(shell dir *.cpp /b/s))

clean:
	del test.exe
	del /q $(OBJS)

CFLAGS += -g 
LIBS += 

%.o: %.cpp
	g++ -c $^ -o $@ $(CFLAGS)

test.exe: $(OBJS)
	g++ $^ -o $@ $(LIBS)
