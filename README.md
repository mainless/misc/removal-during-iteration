# Removal from a vector during iteration

This test is a simplified, isolated model of the Event class.

The vector in Event doesn't care about the ordering of the subscribers, therefore it can use an optimized constant-time removal (instead of shifting all elements forward when removing from an arbitrary position, we can just put the last element in place of the one being removed and shrink the size by 1).

While a loop is iterating through the vector, if we remove an element that is at or before where we are current iterating, then without any special considerations for this situation we will skip over the element that was moved into the removed item's location. 

In the context of a game, this could certainly happen:
- Call an event that many game objects are subscribed to.
- Some of the game objects destroy themselves because of reaching a time limit, going off screen, whatever.
- Game objects that still exist could have their Execute entirely skipped even though they are subscribed and the event was fired, this is unacceptable.

# Excluding added objects from an ongoing iteration 

For events that are currently being fired, which may create additional objects subscribing to the same event, 
the event hasn't technically been fired yet during the lifetime of that new object, so it makes sense to exclude it rather than 
executing its code during the ongoing iteration. 

Example:
- A timer event fires every 2 seconds.
- When executed, subscribers may create new objects that themselves subscribe to the timer event.
- It probably does not make sense to execute new subscribers until the event is fired again 2 seconds later.

If a game developer relies on this exclusion, not having it could even cause an infinite loop.

However a game developer may want to actually a subscriber to execute immediately if an event is still being triggered, so it
may be worth investigating adding a flag to enable the behavior.
